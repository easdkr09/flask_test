# Flask와 react 연동 기본 앱
## Windows 버전 

### Flask api 생성
```
(관리자 권한)
$ python -m venv venv
$ .\venv\Scripts\activate
(venv) $_
```
```
(venv) $ pip install flask python-dotenv
(venv) $ pip install -U flask-cors
```

### app.py 작성 
```python
from flask import Flask
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route('/hello')
def say_hello_world():
    return {'result' : "Hello World"}
```

### .env 작성
```
FLASK_APP=app.py
FLASK_ENV=development
```

### flask run
```
(venv) $ >flask run
 * Serving Flask app "app.py" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 225-527-120
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
```

### React app 생성
```
(다른 터미널 열어서 실행)
$ npx create-react-app react-flask-app
```

### package.json 수정
```
"proxy": "http://localhost:5000" 추가
```

### 통합? 
```javascript
import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  const [placeholder, setPlaceholder] = useState('Hi');

  useEffect(() => {
    fetch('/hello').then(res => res.json()).then(data => {
      setPlaceholder(data.result);
    });
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <p>Flask says {placeholder}</p>
      </header>
    </div>
  );
}

export default App;
```

### react 실행
```
$ npm start
```
### 참고
https://blog.miguelgrinberg.com/post/how-to-create-a-react--flask-project 


https://dev.to/divyajyotiuk/how-to-create-react-app-with-flask-backend-2nf7
#### 각 파트의 디테일한 설명은 추후 추가 예정